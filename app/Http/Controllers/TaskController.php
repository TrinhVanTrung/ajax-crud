<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;

use App\Task;

class TaskController extends Controller
{
    //
    
    public function create(Request $request)
    {
        $task = Task::create($request->all());
   
        return Response::json($task);
        
    }
    public  function update(Request $request,$task_id)
    {
        
        $task = Task::find($task_id);

        $task->task = $request->task;
        $task->description = $request->description;
    
        $task->save();
    
        return Response::json($task);
    }
    
}
